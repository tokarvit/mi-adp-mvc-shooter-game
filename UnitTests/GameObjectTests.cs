﻿using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using FluentAssertions;

using Shooter.Classes;
using Shooter.Models;

namespace UnitTests
{
    [TestClass]
    public class GameObjectTests
    {
        [TestMethod]
        public void PositiveCollideWith()
        {
            // Arrange
            var a = new Ball(0, 0, 10, 10);
            var b = new Ball(9, 9, 10, 10);

            // Act
            var collide = a.CollideWith(b);

            // Assert
            collide.Should().BeTrue();
        }

        [TestMethod]
        public void NegativeCollideWith()
        {
            // Arrange
            var a = new Ball(0, 0, 10, 10);
            var b = new Ball(11, 11, 10, 10);

            // Act
            var collide = a.CollideWith(b);

            // Assert
            collide.Should().BeFalse();
        }

        [TestMethod]
        public void MoveTubeMaximumAngle()
        {
            // Arrange
            var tube = new CannonTube(0,0,0,0, new Point(0,0));
            var cannon = new Cannon(0,0, 10,10, tube, null);

            // Act
            cannon.MoveTubeBy(double.MaxValue);

            // Assert
            cannon.Tube.Angle.Should().Be(GameSettings.MAX_ANGLE);
        }
    }
}
