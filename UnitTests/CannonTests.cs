﻿using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using FluentAssertions;
using Moq;

using Shooter.Classes;
using Shooter.Models;
using Shooter.Classes.GameObjectFactory;
using System.Linq;
using Shooter.Classes.State;

namespace UnitTests
{
    [TestClass]
    public class CannonTests
    {
        private static Mock<IGameObjectFactory> ballFactory;

        [ClassInitialize]
        public static void Initialize(TestContext testContext)
        {
            ballFactory = new Mock<IGameObjectFactory>();
            ballFactory.Setup(f => f.CreateBall(It.IsAny<Point>(), It.IsAny<Point>(), It.IsAny<double>()))
                .Returns(new Ball(1, 1, 0, 0));
        }

        [TestMethod]
        public void SingleModeShoot()
        {
            // Arrange
            var tube = new CannonTube(0, 0, 0, 0, new Point(0, 0));
            var cannon = new Cannon(0, 0, 10, 10, tube, ballFactory.Object);

            // Act & Assert
            var balls = cannon.ShootingMode.Shoot(cannon.Power);

            // Assert
            balls.Should().HaveCount(1);
        }

        [TestMethod]
        public void MultipleModeShoot()
        {
            // Arrange
            var tube = new CannonTube(0, 0, 0, 0, new Point(0, 0));
            var cannon = new Cannon(0, 0, 10, 10, tube, ballFactory.Object);
            var multipleShot = new MultipleShot(ballFactory.Object, cannon);
            cannon.ShootingMode = multipleShot;

            // Act
            var balls = cannon.ShootingMode.Shoot(cannon.Power);

            // Assert
            balls.Should().HaveCount(3);
        }
    }
}
