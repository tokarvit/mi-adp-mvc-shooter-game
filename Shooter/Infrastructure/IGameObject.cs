﻿using Shooter.Classes.Visitor;
using System.Windows;

namespace Shooter.Infrastructure
{
    public interface IGameObject: IRendererVisitable
    {
        Point Position { get; set; }
        Point Size { get; set; }

        Point LeftCenterPoint();
        Point RightCenterPoint();
        Point CenterTopPoint();
        Point CenterBottomPoint();

        Point LeftBottomPoint();
        Point RightBottomPoint();
        Point LeftTopPoint();
        Point RightTopPoint();

        bool CollideWith(IGameObject go);
    }
}
