﻿using Shooter.Classes.GameObjectFactory;
using Shooter.Classes.Observer;
using Shooter.Classes.Proxy;
using Shooter.Controllers;
using Shooter.Infrastructure;
using Shooter.Models;
using Shooter.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Shooter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IGameLoopObservable gameLoop;

        public MainWindow()
        {
            InitializeComponent();
            Closed += MainWindow_Closed;

            var resourceController = new ResourceController();
            var goFactory = GameObjectFactory.CreateFactory(GameObjectFactory.FactoryType.Real, resourceController);
            var renderer = new Renderer(CanvasView, resourceController);
            gameLoop = new LoopController();

            var inputController = new InputController();
            KeyDown += inputController.OnKeyDownHandler;

            var gameModel = new GameModel(renderer, goFactory, gameLoop);
            var gameModelProxy = new GameModelProxy(gameModel);

            var gameController = new GameController(gameModelProxy);
            inputController.AddObserver(gameController);

            gameModelProxy.StartGame();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            gameLoop.StopLoop();
        }
    }
}
