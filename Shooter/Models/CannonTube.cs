﻿using Shooter.Classes.Visitor;
using System.Windows;

namespace Shooter.Models
{
    public class CannonTube : GameObject
    {
        public Point RotateCenter { get; private set; }
        public double Angle { get; set; } = 0;

        public CannonTube(double x, double y, double width, double height,
            Point rotateCenter) : base(x, y, width, height)
        {
            RotateCenter = rotateCenter;
        }

        public void MoveTubeBy(double angle)
        {
            Angle += angle;
        }

        public override void Accept(IRendererVisitor renderer)
        {
            renderer.Visit(this);
        }
    }
}
