﻿using Shooter.Classes.Visitor;
using Shooter.Infrastructure;
using System;
using System.Windows;

namespace Shooter.Models
{
    public class Bat : GameObject
    {
        public Point Velocity { get; private set; }
        public double Speed { get; set; } 
        public long Lifetime{ get; private set; }
        public int Life { get; set; } = 2;
        public Bat(double x, double y, double width, double height) 
            : base(x, y, width, height)
        {
        }

        public override void Accept(IRendererVisitor renderer)
        {
            Lifetime++;
            renderer.Visit(this);
        }

        public void MoveVelocityBy(double x, double y)
        {
            Velocity = new Point(Velocity.X + x, Velocity.Y + y);
        }

        public void Move()
        {
            MoveBy(Velocity.X * Speed, Velocity.Y * Speed);
        }
    }
}
