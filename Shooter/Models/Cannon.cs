﻿using System.Collections.Generic;

using Shooter.Classes.GameObjectFactory;
using Shooter.Classes.Visitor;
using Shooter.Classes.State;
using Shooter.Classes;

namespace Shooter.Models
{
    public class Cannon : GameObject
    {
        public CannonTube Tube { get; set; }
        public double Power { get; set; } = GameSettings.INIT_POWER;
        public IShootingMode ShootingMode { get; set; }

        public Cannon(double x, double y, double width, double height,
            CannonTube tube, IGameObjectFactory goFactory) : base(x, y, width, height)
        {
            Tube = tube;
            ShootingMode = new SingleShot(goFactory, this);
        }

        public void MoveTubeBy(double angle)
        {
            Tube.Angle -= angle;
            if (Tube.Angle > GameSettings.MAX_ANGLE) Tube.Angle = GameSettings.MAX_ANGLE;
            if (Tube.Angle < GameSettings.MIN_ANGLE) Tube.Angle = GameSettings.MIN_ANGLE;
        }

        public override void Accept(IRendererVisitor renderer)
        {
            Tube.Accept(renderer);
            renderer.Visit(this);
        }

        public List<Ball> Shoot() => ShootingMode.Shoot(Power);

        public void SwitchShootingMode() => ShootingMode = ShootingMode.NextMode();

        public void IncreasePowerBy(double p)
        {
            Power += p;
            if (Power > GameSettings.MAX_POWER) Power = GameSettings.MAX_POWER;
            if (Power < 1) Power = 1;
        }
    }
}
