﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shooter.Classes.Visitor;

namespace Shooter.Models
{
    public class GameInfo : GameObject
    {
        public int Score { get; set; }
        public int TubeAngle { get; set; }
        public int Power { get; set; }
        public string MovingStrategy { get; set; }
        public string ShootingMode { get; set; }

        public override void Accept(IRendererVisitor renderer)
        {
            renderer.Visit(this);
        }
    }
}
