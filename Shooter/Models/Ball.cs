﻿using Shooter.Classes.Strategy;
using Shooter.Classes.Visitor;
using System.Windows;

namespace Shooter.Models
{
    public class Ball : GameObject
    {
        public Point Velocity { get; private set; }
        public double Speed { get; set; } 

        public Ball(double x, double y, double width, double height) 
            : base(x, y, width, height)
        {
        }

        public override void Accept(IRendererVisitor renderer)
        {
            renderer.Visit(this);
        }

        public void MoveVelocityBy(double x, double y)
        {
            Velocity = new Point(Velocity.X + x, Velocity.Y + y);
        }
        
        public void Move(IMovingStrategy movingStrategy)
        {
            movingStrategy.Move(this);
        }
    }
}
