﻿using Shooter.Classes.Visitor;

namespace Shooter.Models
{
    public class Background : GameObject
    {
        public Background(double x, double y, double width, double height)
             : base(x, y, width, height)
        {
        }

        public override void Accept(IRendererVisitor renderer)
        {
            renderer.Visit(this);
        }
    }
}
