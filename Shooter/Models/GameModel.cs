﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

using Shooter.Classes;
using Shooter.Classes.CommandAndMemento;
using Shooter.Classes.GameObjectFactory;
using Shooter.Classes.Observer;
using Shooter.Classes.Proxy;
using Shooter.Classes.State;
using Shooter.Classes.Strategy;
using Shooter.Classes.Visitor;
using Shooter.Infrastructure;
using Shooter.Models;

namespace Shooter.Controllers
{
    public class GameModel: IGameLoopObserver, IGameModel
    {
        private readonly IRendererVisitor renderer;
        private readonly IGameObjectFactory goFactory;
        private readonly IGameLoopObservable gameLoop;
                
        private List<Ball> balls = new List<Ball>();
        private List<Bat> bats = new List<Bat>();
        private Background background;
        private Cannon cannon;
        private GameInfo info;

        private long lastEnemySpawnTime = 0;

        private IMovingStrategy[] movingStrategies;
        private int movingStrategyIndex;
        private Queue<AbstractGameCommand> commandsToExec = new Queue<AbstractGameCommand>();
        private Stack<AbstractGameCommand> executedCommands = new Stack<AbstractGameCommand>();

        public GameModel(IRendererVisitor renderer, IGameObjectFactory goFactory, IGameLoopObservable gameLoop)
        {
            this.renderer = renderer;
            this.goFactory = goFactory;
            this.gameLoop = gameLoop;

            gameLoop.AddObserver(this);

            movingStrategies = new IMovingStrategy[] { new DefaultMoving(), new PhysicBasedMoving()};
        }
        
        public void StartGame()
        {
            background = goFactory.CreateBackground();
            cannon = goFactory.CreateCannon(50, GameSettings.GROUND_LEVEL);
            InitInfo();

            gameLoop.StartLoop();
        }

        #region IGameLoopObserver
        public void PreUpdate(long time)
        {
            renderer.Clear();
            RemoveObjectOutOfGameScene();
            HandleCollisions();
        }

        public void OnUpdate(long time)
        {
            ExecuteCommands();
            SpawnEnemy(time);
            balls.ForEach(b => b.Move(movingStrategies[movingStrategyIndex]));
            bats.ForEach(b => b.Move());
        }

        public void PostUpdate(long time)
        {
            Render();
        }
        #endregion

        public void MoveTube(int dir)
        {
            cannon.MoveTubeBy(-dir * GameSettings.MOVE_TUBE_SPEED);
            info.TubeAngle = (int)cannon.Tube.Angle;
        }

        public void IncreasePower(int dir)
        {
            cannon.IncreasePowerBy(dir * GameSettings.INCREASE_POWER_BY_PRESS);
            info.Power = (int)cannon.Power;
        }

        public void Shoot()
        {
            var newBalls = cannon.Shoot();
            newBalls.ForEach(b =>
            {
                balls.Add(b);
            });
        }

        public void SwitchShootingMode()
        {
            cannon.SwitchShootingMode();
            info.ShootingMode = cannon.ShootingMode.GetType().Name;
        }

        private void Render()
        {
            GetAllGameObjects().ForEach(go => go.Accept(renderer));
        }

        private void RemoveObjectOutOfGameScene()
        {
            var ballsToRemove = new List<Ball>();
            balls.ForEach(b => {
                if(IsObjectOutOfScreen(b))
                {
                    ballsToRemove.Add(b);
                }
            });
            balls = balls.Except(ballsToRemove).ToList();

            var batsToRemove = new List<Bat>();
            bats.ForEach(b => {
                if (IsObjectOutOfScreen(b))
                {
                    batsToRemove.Add(b);
                }
            });
            bats = bats.Except(batsToRemove).ToList();
        }

        private bool IsObjectOutOfScreen(IGameObject go)
        {
            double offset = 50;

            if (go.Position.X < background.LeftBottomPoint().X - offset
                || go.Position.X > background.RightTopPoint().X + offset
                || go.Position.Y < background.LeftBottomPoint().Y - offset
                || go.Position.Y > background.RightTopPoint().Y + offset)
            {
                return true;
            }

            return false;
        }

        private List<IGameObject> GetAllGameObjects()
        {
            var gameObjects = new List<IGameObject>();
            gameObjects.Add(background);
            gameObjects.AddRange(balls);
            gameObjects.Add(cannon);
            gameObjects.AddRange(bats);
            gameObjects.Add(info);

            return gameObjects;
        }

        private void HandleCollisions()
        {
            var ballsToRemove = new List<Ball>();
            var batsToRemove = new List<Bat>();

            foreach(var ball in balls)
            {
                foreach (var bat in bats)
                {
                    if (ball.CollideWith(bat))
                    {
                        ballsToRemove.Add(ball);
                        bat.Life--;
                        if(bat.Life <= 0)
                            batsToRemove.Add(bat);
                        info.Score++;
                    }
                }
            }

            balls = balls.Except(ballsToRemove).ToList();
            bats = bats.Except(batsToRemove).ToList();
        }

        private void SpawnEnemy(long time)
        {
            if(bats.Count < GameSettings.MAX_ENEMY
                && time - lastEnemySpawnTime > GameSettings.MIN_TIME_TO_SPAWN_ENEMY)
            {
                lastEnemySpawnTime = time;
                bats.Add(goFactory.CreateBat(background.RightCenterPoint()));
            }
        }

        public void SwitchMovingStrategy()
        {
            movingStrategyIndex = (movingStrategyIndex + 1) % movingStrategies.Length;
            info.MovingStrategy = movingStrategies[movingStrategyIndex].GetType().Name;
        }

        private void InitInfo()
        {
            info = new GameInfo()
            {
                Position = new Point(5, 5),
                Power = (int)cannon.Power,
                MovingStrategy = movingStrategies[movingStrategyIndex].GetType().Name,
                ShootingMode = cannon.ShootingMode.GetType().Name,
                TubeAngle = (int)cannon.Tube.Angle
            };
        }

        #region CommandAndMemento
        public object CreateMemento()
        {
            return new Memento()
            {
                ShootingMode = cannon.ShootingMode,
                Angle = cannon.Tube.Angle,
                Power = cannon.Power,
                MovingStrategy = movingStrategyIndex
            };
        }

        public void SetMemento(object mementoObj)
        {
            var memento = (Memento)mementoObj;

            cannon.ShootingMode = memento.ShootingMode;
            cannon.Tube.Angle = memento.Angle;
            cannon.Power = memento.Power;
            movingStrategyIndex = memento.MovingStrategy;

            InitInfo();
        }

        private void ExecuteCommands()
        {
            while(commandsToExec.Count > 0)
            {
                var cmd = commandsToExec.Dequeue();
                cmd.Execute();
                executedCommands.Push(cmd);
                if (executedCommands.Count > GameSettings.MAX_UNDO_COMMANDS)
                    executedCommands.Pop();
            }
        }

        public void RegisterCmd(AbstractGameCommand cmd)
        {
            commandsToExec.Enqueue(cmd);
        }

        public void UndoLastCommand()
        {
            if (executedCommands.Count > 0)
            {
                var cmd = executedCommands.Pop();
                cmd.Unexecute();
            }
        }

        private class Memento
        {
            public IShootingMode ShootingMode { get; set; }
            public int MovingStrategy { get; set; }
            public double Power { get; set; }
            public double Angle { get; set; }
        }
        #endregion

    }
}
