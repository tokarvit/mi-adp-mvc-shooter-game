﻿using Shooter.Classes.Visitor;
using Shooter.Infrastructure;
using System.Windows;

namespace Shooter.Models
{
    public abstract class GameObject : IGameObject
    {
        public Point Position { get; set; }
        public Point Size { get; set; }

        public GameObject()
        {
            Position = new Point(0, 0);
        }

        public GameObject(double x, double y, double width, double height)
        {
            Position = new Point(x, y);
            Size = new Point(width, height);
        }

        public void MoveBy(double x, double y)
        {
            Position = new Point(Position.X + x, Position.Y + y);
        }
        
        public Point LeftCenterPoint() => new Point(Position.X, Position.Y + Size.Y / 2);
        public Point RightCenterPoint() => new Point(Position.X + Size.X, Position.Y + Size.Y / 2);
        public Point CenterTopPoint() => new Point(Position.X + Size.X / 2, Position.Y + Size.Y);
        public Point CenterBottomPoint() => new Point(Position.X + Size.X / 2, Position.Y);

        public Point LeftBottomPoint() => new Point(Position.X, Position.Y);
        public Point RightBottomPoint() => new Point(Position.X + Size.X, Position.Y);
        public Point LeftTopPoint() => new Point(Position.X, Position.Y + Size.Y);
        public Point RightTopPoint() => new Point(Position.X + Size.X, Position.Y + Size.Y);

        public abstract void Accept(IRendererVisitor renderer);

        public bool CollideWith(IGameObject go)
        {
            if (IsPointInsideBoundary(LeftTopPoint(), go)
                || IsPointInsideBoundary(LeftBottomPoint(), go)
                || IsPointInsideBoundary(RightTopPoint(), go)
                || IsPointInsideBoundary(RightBottomPoint(), go))
                return true;

            if (IsPointInsideBoundary(go.LeftTopPoint(), this)
                || IsPointInsideBoundary(go.LeftBottomPoint(), this)
                || IsPointInsideBoundary(go.RightTopPoint(), this)
                || IsPointInsideBoundary(go.RightBottomPoint(), this))
                return true;

            return false;
        }

        private bool IsPointInsideBoundary(Point point, IGameObject go)
            => point.X >= go.LeftTopPoint().X && point.Y <= go.LeftTopPoint().Y
            && point.X <= go.RightBottomPoint().X && point.Y >= go.RightBottomPoint().Y;
    } 

}
