﻿using Shooter.Classes.Visitor;
using Shooter.Controllers;
using Shooter.Models;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;

namespace Shooter.Views
{
    public class Renderer: IRendererVisitor
    {
        private readonly Canvas canvas;
        private readonly ResourceController resources;

        private const double Cannon_Tube_Width = 150;
        
        public Renderer(Canvas canvas, ResourceController resourceController)
        {
            this.canvas = canvas;
            this.resources = resourceController;
        }

        public void Visit(Cannon cannon)
        {
            AddSprite(resources.GetBitmapByName("cannonBase"), cannon.Position, cannon.Size);
        }

        public void Visit(CannonTube tube)
        {
            var tubeSprite = AddSprite(resources.GetBitmapByName("cannonTube"),
                tube.Position, tube.Size);
            tubeSprite.RenderTransformOrigin = tube.RotateCenter;
            tubeSprite.RenderTransform = new RotateTransform(-tube.Angle);
        }

        public void Visit(Background background)
        {
            AddSprite(resources.GetBitmapByName("background"), background);
        }

        public void Visit(Ball ball)
        {
            AddSprite(resources.GetBitmapByName("cannonBall"), ball);
        }

        public void Visit(Bat bat)
        {
            if(bat.Lifetime % 20 > 10)
                AddSprite(resources.GetBitmapByName("bat_1"), bat);
            else
                AddSprite(resources.GetBitmapByName("bat_2"), bat);
        }

        public void Visit(GameInfo info)
        {
            AddText($"Score: {info.Score}   Tube angle: {info.TubeAngle}   Power: {info.Power}    " 
                + $"Shooting mode: {info.ShootingMode}   Moving strategy: {info.MovingStrategy}", 
                info.Position);
        }

        public void Clear() => canvas.Children.Clear();

        private Image AddSprite(BitmapImage bitmapImage, GameObject go)
            => AddSprite(bitmapImage, go.Position, go.Size);

        private Image AddSprite(BitmapImage bitmapImage, Point position, Point size)
        {
            var image = new Image
            {
                Source = bitmapImage,
                Width = size.X,
                Height = size.Y
            };

            AddUIElement(image, position);
            return image;
        }

        private TextBlock AddText(string text, Point position)
        {
            var textElem = new TextBlock()
            {
                Text = text,
                FontSize = 20,
                Foreground = new SolidColorBrush(Colors.White)
            };

            AddUIElement(textElem, position);
            return textElem;
        }

        private void AddUIElement(UIElement uIElement, Point pos)
        {
            Canvas.SetLeft(uIElement, pos.X);
            Canvas.SetBottom(uIElement, pos.Y);
            canvas.Children.Add(uIElement);
        }
    }
}
