﻿using Shooter.Classes.Observer;
using System.Collections.Generic;
using System.Windows.Input;

namespace Shooter.Controllers
{
    public class InputController : IInputObservable
    {
        private readonly HashSet<IInputObserver> observers;

        public InputController()
        {
            observers = new HashSet<IInputObserver>();
        }

        public void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    foreach (var o in observers) o.OnVertical(1);
                    break;
                case Key.Down:
                    foreach (var o in observers) o.OnVertical(-1);
                    break;
                case Key.Space:
                    foreach (var o in observers) o.OnSpace();
                    break;
                case Key.Tab:
                    foreach (var o in observers) o.OnTab();
                    break;
                case Key.Q:
                    foreach (var o in observers) o.OnQ();
                    break;
                case Key.Left:
                    foreach (var o in observers) o.OnHorizontal(-1);
                    break;
                case Key.Right:
                    foreach (var o in observers) o.OnHorizontal(1);
                    break;
                case Key.Z:
                    if (Keyboard.Modifiers == ModifierKeys.Control) {
                        foreach (var o in observers) o.OnCtrlZ();
                    }
                    break;
            }
        }

        public void AddObserver(IInputObserver observer) => observers.Add(observer);

        public void RemoveObserver(IInputObserver observer) => observers.Remove(observer);
    }
}
