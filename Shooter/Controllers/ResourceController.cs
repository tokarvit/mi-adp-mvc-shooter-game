﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Shooter.Controllers
{
    public class ResourceController
    {
        private Dictionary<string, BitmapImage> sprites;

        public ResourceController()
        {
            sprites = new Dictionary<string, BitmapImage>();

            sprites.Add("background", GetBitmapImage("background"));
            sprites.Add("cannonBall", GetBitmapImage("cannonBall"));
            sprites.Add("cannonBase", GetBitmapImage("cannonBase"));
            sprites.Add("cannonTube", GetBitmapImage("cannonTube"));
            sprites.Add("collisionBox", GetBitmapImage("collisionBox"));
            sprites.Add("debugCircle", GetBitmapImage("debugCircle"));
            sprites.Add("bat_1", GetBitmapImage("bat_1"));
            sprites.Add("bat_2", GetBitmapImage("bat_2"));
        }

        public BitmapImage GetBitmapByName(string name) => sprites[name];

        private BitmapImage GetBitmapImage(string imageName)
        {
            var uri = new Uri("/Resources/Sprites/" + imageName + ".png", UriKind.Relative);
            var info = Application.GetResourceStream(uri);
            
            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = info.Stream;
            bmp.EndInit();

            return bmp;
        }
    }
}
