﻿using Shooter.Classes.CommandAndMemento;
using Shooter.Classes.Observer;
using Shooter.Classes.Proxy;

namespace Shooter.Controllers
{
    public class GameController: IInputObserver
    {
        private IGameModel gameModel;
        
        public GameController(IGameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        #region IInputObserver
        public void OnVertical(int dir)
            => gameModel.RegisterCmd(new MoveTubeCommand(gameModel, dir));

        public void OnHorizontal(int dir)
            => gameModel.RegisterCmd(new IncreasePowerCommand(gameModel, dir));

        public void OnSpace() => gameModel.Shoot();

        public void OnTab()
            => gameModel.RegisterCmd(new SwitchShootingModeCommand(gameModel));

        public void OnQ() 
            => gameModel.RegisterCmd(new SwitchMovingStrategyCommand(gameModel));

        public void OnCtrlZ()
            => gameModel.UndoLastCommand();
        #endregion
    }
}
