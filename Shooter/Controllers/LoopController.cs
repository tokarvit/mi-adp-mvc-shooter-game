﻿using Shooter.Classes.Observer;
using System;
using System.Collections.Generic;
using System.Timers;
using System.Windows;

namespace Shooter.Controllers
{
    public class LoopController: IGameLoopObservable
    {
        private const int Frame_Rate = 1000 / 30;

        private Timer timer;
        private List<IGameLoopObserver> observers;

        public LoopController()
        {
            observers = new List<IGameLoopObserver>();

            timer = new Timer();
            timer.Interval = Frame_Rate;
            timer.Elapsed += new ElapsedEventHandler(OnTimerTick);
        }

        public void StartLoop() => timer.Start();
        
        public void StopLoop() => timer.Stop();

        public void AddObserver(IGameLoopObserver observer)
        {
            if(!observers.Contains(observer)) observers.Add(observer);
        }

        public void NotifyAll()
        {
            long time = DateTimeOffset.Now.ToUnixTimeSeconds();
            observers.ForEach(o => o.PreUpdate(time));
            observers.ForEach(o => o.OnUpdate(time));
            observers.ForEach(o => o.PostUpdate(time));
        }

        public void RemoveObserver(IGameLoopObserver observer)
        {
            observers.Remove(observer);
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                NotifyAll();
            });
        }
    }
}
