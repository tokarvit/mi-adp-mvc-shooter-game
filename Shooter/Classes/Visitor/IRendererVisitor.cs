﻿using Shooter.Models;

namespace Shooter.Classes.Visitor
{
    public interface IRendererVisitor
    {
        void Visit(Cannon cannon);
        void Visit(CannonTube tube);
        void Visit(Ball ball);
        void Visit(Bat bat);
        void Visit(Background background);
        void Visit(GameInfo info);
        void Clear(); 
    }
}
