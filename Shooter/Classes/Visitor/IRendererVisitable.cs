﻿namespace Shooter.Classes.Visitor
{
    public interface IRendererVisitable
    {
        void Accept(IRendererVisitor renderer);
    }
}
