﻿using Shooter.Models;

namespace Shooter.Classes.Strategy
{
    public class DefaultMoving : IMovingStrategy
    {
        public void Move(Ball ball)
        {
            ball.MoveBy(
                ball.Velocity.X * ball.Speed, 
                ball.Velocity.Y * ball.Speed);
        }
    }
}
