﻿using Shooter.Models;

namespace Shooter.Classes.Strategy
{
    public interface IMovingStrategy
    {
        void Move(Ball ball);
    }
}
