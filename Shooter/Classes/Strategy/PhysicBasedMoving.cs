﻿using Shooter.Models;

namespace Shooter.Classes.Strategy
{
    public class PhysicBasedMoving : IMovingStrategy
    {
        public void Move(Ball ball)
        {
            ball.MoveVelocityBy(0, GameSettings.GRAVITY);
            ball.Speed -= GameSettings.SPEED_FALL;
            ball.MoveBy(
                ball.Velocity.X * ball.Speed,
                ball.Velocity.Y * ball.Speed);
        }
    }
}
