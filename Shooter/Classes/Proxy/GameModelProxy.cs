﻿using Shooter.Classes.CommandAndMemento;

namespace Shooter.Classes.Proxy
{
    public class GameModelProxy : IGameModel
    {
        private IGameModel subject;

        public GameModelProxy(IGameModel subject)
        {
            this.subject = subject;
        }

        public void MoveTube(int dir) => subject.MoveTube(dir);

        public void IncreasePower(int dir) => subject.IncreasePower(dir);

        public void Shoot() => subject.Shoot();

        public void StartGame() => subject.StartGame();

        public void SwitchShootingMode() => subject.SwitchShootingMode();

        public void SwitchMovingStrategy() => subject.SwitchMovingStrategy();

        public object CreateMemento() => subject.CreateMemento();

        public void SetMemento(object memento) => subject.SetMemento(memento);

        public void RegisterCmd(AbstractGameCommand cmd) => subject.RegisterCmd(cmd);

        public void UndoLastCommand() => subject.UndoLastCommand();
    }
}
