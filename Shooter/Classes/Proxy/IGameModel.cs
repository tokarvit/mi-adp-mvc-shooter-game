﻿using Shooter.Classes.CommandAndMemento;

namespace Shooter.Classes.Proxy
{
    public interface IGameModel
    {
        void StartGame();
        void MoveTube(int dir);
        void IncreasePower(int dir);
        void Shoot();
        void SwitchShootingMode();
        void SwitchMovingStrategy();
        object CreateMemento();
        void SetMemento(object memento);
        void RegisterCmd(AbstractGameCommand cmd);
        void UndoLastCommand();
    }
}
