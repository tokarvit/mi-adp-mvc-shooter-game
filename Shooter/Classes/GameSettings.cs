﻿namespace Shooter.Classes
{
    public static class GameSettings
    {
        public static readonly double MAX_POWER = 15;
        public static readonly double INIT_POWER = 5;
        public static readonly double INCREASE_POWER_BY_PRESS = 1;
        public static readonly double MOVE_TUBE_SPEED = 2;
        public static readonly double MAX_ANGLE = 90;
        public static readonly double MIN_ANGLE = 0;

        public static readonly double GRAVITY = -0.02;
        public static readonly double SPEED_FALL = -0.02;

        public static readonly double GROUND_LEVEL = 60;
        public static readonly int MAX_ENEMY = 5;
        public static readonly int MIN_TIME_TO_SPAWN_ENEMY = 5;

        public static readonly int MAX_UNDO_COMMANDS = 50;

        public static readonly double MULTIPLE_SHOT_INTERVAL = 2;
        public static readonly double SINGLE_SHOT_INTERVAL = 1;
    }
}
