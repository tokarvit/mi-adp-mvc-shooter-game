﻿using Shooter.Controllers;
using Shooter.Models;
using System.Windows;

namespace Shooter.Classes.GameObjectFactory
{
    public abstract class GameObjectFactory : IGameObjectFactory
    {
        public enum FactoryType { Real, Standart }

        protected ResourceController resourceController;
        
        public static IGameObjectFactory CreateFactory(FactoryType type, ResourceController resourceController)
        {
            switch (type)
            {
                //case FactoryType.Real: return new RealGameObjectFactory();
                case FactoryType.Standart: return new StandartGameObjectFactory(resourceController);
                default: return new StandartGameObjectFactory(resourceController);
            }
        }

        protected double ImageAspectRatio(string name)
        {
            var img = resourceController.GetBitmapByName(name);
            return img.Height / img.Width;
        }

        public abstract Cannon CreateCannon(double x, double y);
        public abstract Background CreateBackground();
        public abstract Ball CreateBall(Point position, Point direction, double initSpeed);
        public abstract Bat CreateBat(Point position);
    }
}
