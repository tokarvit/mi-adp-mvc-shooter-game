﻿using Shooter.Models;
using System.Windows;

namespace Shooter.Classes.GameObjectFactory
{
    public interface IGameObjectFactory
    {
        Cannon CreateCannon(double x, double y);
        Background CreateBackground();
        Ball CreateBall(Point position, Point direction, double initSpeed);
        Bat CreateBat(Point position);
    }
}
