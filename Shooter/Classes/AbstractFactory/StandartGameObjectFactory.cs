﻿using Shooter.Controllers;
using Shooter.Models;
using Shooter.Properties;
using Shooter.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Shooter.Classes.GameObjectFactory
{
    public class StandartGameObjectFactory: GameObjectFactory
    {
        public StandartGameObjectFactory(ResourceController resourceController)
        {
            this.resourceController = resourceController;
        }

        public override Background CreateBackground()
        {
            double width = 1300;
            return new Background(0, 0, width, ImageAspectRatio("background") * width);
        }

        public override Cannon CreateCannon(double x, double y)
        {
            double cannonWidth = 100;
            double cannonHeight = ImageAspectRatio("cannonBase") * cannonWidth;

            double tubeWidth = 150; 
            double tubeHeight = ImageAspectRatio("cannonBase") * tubeWidth;

            var tube = new CannonTube(x, y + cannonHeight - tubeHeight / 2,

                tubeWidth, tubeHeight, new Point(cannonWidth / 2 / tubeWidth, 0.5));
            var cannon = new Cannon(x, y, cannonWidth, cannonHeight, tube, this);

            return cannon;
        }

        public override Ball CreateBall(Point position, Point direction, double initSpeed)
        {
            double width = 20;
            double height = ImageAspectRatio("cannonBase") * width;

            var ball = new Ball(position.X, position.Y, width, height);
            ball.MoveVelocityBy(direction.X, direction.Y);
            ball.Speed = initSpeed;
            
            return ball;
        }

        public override Bat CreateBat(Point position)
        {
            double width = 70;
            double height = ImageAspectRatio("cannonBase") * width;

            var bat = new Bat(position.X, position.Y, width, height);
            bat.MoveVelocityBy(-1, 0);
            bat.Speed = 6;

            return bat;
        }
    }
}
