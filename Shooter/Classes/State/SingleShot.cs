﻿using Shooter.Infrastructure;
using Shooter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Shooter.Tools;
using Shooter.Classes.GameObjectFactory;

namespace Shooter.Classes.State
{
    public class SingleShot : ShootingStrategyBase
    {
        public SingleShot(IGameObjectFactory goFactory, Cannon cannon) 
            : base(goFactory, cannon)
        {
            Interval = GameSettings.SINGLE_SHOT_INTERVAL;
        }

        public override List<Ball> Shoot(double initSpeed)
        {
            if (!UpdateInterval()) return new List<Ball>();

            var position = InstantiatePosition();
            var rotateCenter = RotateCenterPosition();
            var direction = new Point(position.X - rotateCenter.X, 
                position.Y - rotateCenter.Y).Normalize();
            
            return new List<Ball>() {
                goFactory.CreateBall(position, direction, initSpeed)
            };
        }

        public override IShootingMode NextMode()
        {
            return new MultipleShot(goFactory, cannon);
        }
    }
}
