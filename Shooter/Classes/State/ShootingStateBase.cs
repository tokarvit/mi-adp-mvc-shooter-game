﻿using Shooter.Classes.GameObjectFactory;
using Shooter.Models;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Shooter.Classes.State
{
    public abstract class ShootingStrategyBase : IShootingMode
    {
        public double Interval { get; set; } = 1;
        
        protected IGameObjectFactory goFactory;
        protected Cannon cannon;

        private long lastShoot;

        protected ShootingStrategyBase(IGameObjectFactory goFactory, Cannon cannon)
        {
            this.goFactory = goFactory;
            this.cannon = cannon;
        }

        public abstract List<Ball> Shoot(double initSpeed);
        public abstract IShootingMode NextMode();

        protected Point RotateCenterPosition()
        {
            return new Point(
                cannon.Tube.LeftCenterPoint().X + cannon.Tube.RotateCenter.X * cannon.Tube.Size.X,
                cannon.Tube.CenterBottomPoint().Y + cannon.Tube.RotateCenter.Y * cannon.Tube.Size.Y
            );
        }

        protected Point InstantiatePosition(double tubeAngleOffset = 0)
        {
            double r = cannon.Tube.Size.X * (1 - cannon.Tube.RotateCenter.X);
            double angleInDeg = (cannon.Tube.Angle + tubeAngleOffset) * Math.PI / 180;

            var rotateCenter = RotateCenterPosition();

            return new Point(
                rotateCenter.X + r * Math.Cos(angleInDeg),
                rotateCenter.Y + r * Math.Sin(angleInDeg));
        }

        protected bool UpdateInterval()
        {
            if (DateTimeOffset.UtcNow.ToUnixTimeSeconds() - lastShoot > Interval)
            {
                lastShoot = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                return true;
            }

            return false;
        }
    }
}
