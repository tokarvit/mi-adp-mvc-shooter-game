﻿using Shooter.Classes.GameObjectFactory;
using Shooter.Models;
using Shooter.Tools;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Shooter.Classes.State
{
    public class MultipleShot : ShootingStrategyBase
    {
        public MultipleShot(IGameObjectFactory goFactory, Cannon cannon) 
            : base(goFactory, cannon)
        {
            Interval = GameSettings.MULTIPLE_SHOT_INTERVAL;
        }

        public override List<Ball> Shoot(double initSpeed)
        {
            var newBalls = new List<Ball>();
            if (!UpdateInterval()) return newBalls;

            var position = InstantiatePosition();
            var rotateCenter = RotateCenterPosition();
            var direction = new Point(position.X - rotateCenter.X,
               position.Y - rotateCenter.Y).Normalize();

            // Middle ball
            var ball = goFactory.CreateBall(position, direction, initSpeed);
            newBalls.Add(ball);

            // Upper ball
            var offsetPos = InstantiatePosition(10);
            direction = new Point(offsetPos.X - rotateCenter.X,
               offsetPos.Y - rotateCenter.Y).Normalize();
            ball = goFactory.CreateBall(position, direction, initSpeed);
            newBalls.Add(ball);

            // Bottom ball
            offsetPos = InstantiatePosition(-10);
            direction = new Point(offsetPos.X - rotateCenter.X,
               offsetPos.Y - rotateCenter.Y).Normalize();
            ball = goFactory.CreateBall(position, direction, initSpeed);
            newBalls.Add(ball);

            return newBalls;
        }

        public override IShootingMode NextMode()
        {
            return new SingleShot(goFactory, cannon);
        }
    }
}
