﻿using Shooter.Models;
using System.Collections.Generic;

namespace Shooter.Classes.State
{
    public interface IShootingMode
    {
        double Interval { get; set; }
        List<Ball> Shoot(double initSpeed);
        IShootingMode NextMode();
    }
}
