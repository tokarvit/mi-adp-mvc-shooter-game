﻿using Shooter.Classes.Proxy;

namespace Shooter.Classes.CommandAndMemento
{
    public class IncreasePowerCommand : AbstractGameCommand
    {
        private int direction;

        public IncreasePowerCommand(IGameModel subject, int direction) : base(subject)
        {   
            this.direction = direction;
        }

        protected override void BaseExecute()
        {
            subject.IncreasePower(direction);
        }
    }
}
