﻿using Shooter.Classes.Proxy;

namespace Shooter.Classes.CommandAndMemento
{
    public class SwitchMovingStrategyCommand : AbstractGameCommand
    {
        public SwitchMovingStrategyCommand(IGameModel subject) : base(subject)
        {   
        }

        protected override void BaseExecute()
        {
            subject.SwitchMovingStrategy();
        }
    }
}
