﻿using Shooter.Classes.Proxy;

namespace Shooter.Classes.CommandAndMemento
{
    public class SwitchShootingModeCommand : AbstractGameCommand
    {
        public SwitchShootingModeCommand(IGameModel subject) : base(subject)
        {   
        }

        protected override void BaseExecute()
        {
            subject.SwitchShootingMode();
        }
    }
}
