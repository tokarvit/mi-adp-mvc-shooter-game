﻿using Shooter.Classes.Proxy;

namespace Shooter.Classes.CommandAndMemento
{
    public abstract class AbstractGameCommand
    {
        protected IGameModel subject;
        protected object memento;

        public AbstractGameCommand(IGameModel subject)
        {
            this.subject = subject;
        }

        public void Execute()
        {
            memento = subject.CreateMemento();
            BaseExecute();
        }

        protected abstract void BaseExecute();

        public void Unexecute()
        {
            subject.SetMemento(memento);
        }
    }
}
