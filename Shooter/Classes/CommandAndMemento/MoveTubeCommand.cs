﻿using Shooter.Classes.Proxy;

namespace Shooter.Classes.CommandAndMemento
{
    public class MoveTubeCommand : AbstractGameCommand
    {
        private int direction;

        public MoveTubeCommand(IGameModel subject, int direction) : base(subject)
        {   
            this.direction = direction;
        }

        protected override void BaseExecute()
        {
            subject.MoveTube(direction);
        }
    }
}
