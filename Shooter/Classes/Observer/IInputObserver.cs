﻿namespace Shooter.Classes.Observer
{
    public interface IInputObserver
    {
        void OnVertical(int dir);
        void OnHorizontal(int dir);
        void OnSpace();
        void OnTab();
        void OnQ();
        void OnCtrlZ();
    }
}
