﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooter.Classes.Observer
{
    public interface IGameLoopObservable
    {
        void StartLoop();
        void StopLoop();
        void AddObserver(IGameLoopObserver observer);
        void RemoveObserver(IGameLoopObserver observer);
        void NotifyAll();
    }
}
