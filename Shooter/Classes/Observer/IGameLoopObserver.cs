﻿namespace Shooter.Classes.Observer
{
    public interface IGameLoopObserver
    {
        void PreUpdate(long time);
        void OnUpdate(long time);
        void PostUpdate(long time);
    }
}
