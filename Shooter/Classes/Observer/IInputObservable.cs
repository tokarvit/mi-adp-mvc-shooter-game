﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooter.Classes.Observer
{
    public interface IInputObservable
    {
        void AddObserver(IInputObserver observer);
        void RemoveObserver(IInputObserver observer);
    }
}
