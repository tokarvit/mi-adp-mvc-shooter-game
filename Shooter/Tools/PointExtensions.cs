﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Shooter.Tools
{
    public static class PointExtensions
    {
        public static double Magnitude(this Point point)
            => Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));

        public static Point Normalize(this Point point)
            => new Point(point.X / point.Magnitude(), point.Y / point.Magnitude());
    }
}
