# MVC Shooter
This game is written using MVC pattern and WPF.

### Keyboard settings:
+ Left/Right arrow - decrease/increase cannon force
+ Up/Down arrow - increase/decrease cannon angle
+ Space - shoot
+ Tab - switch shooting mode
+ Q - switch moving strategy(on/off gravity)
+ Ctrl-Z - undo last action